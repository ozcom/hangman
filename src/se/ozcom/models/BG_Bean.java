package se.ozcom.models;

import java.io.Serializable;

public class BG_Bean implements Serializable {
	private static final long serialVersionUID = 1L;
	
private int r;
 private int g;
 private int b;

 public BG_Bean(){
	 r = 255;
	 g = 0;
	 b = 0;
 }

public int getR() {
	return r;
}

public void setR(int r) {
	this.r = r;
}

public int getG() {
	return g;
}

public void setG(int g) {
	this.g = g;
}

public int getB() {
	return b;
}

public void setB(int b) {
	this.b = b;
}
 
 
}

