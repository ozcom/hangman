<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="bg" class="se.ozcom.models.BG_Bean" scope="application"></jsp:useBean>    
<jsp:setProperty name="bg" property="r" param="red"/>
<jsp:setProperty name="bg" property="g" param="green"/>
<jsp:setProperty name="bg" property="b" param="blue"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Testing BG Bean</title>
<link rel="stylesheet" href="./css/Name.css" />
</head>
<body style="background: rgb(<jsp:getProperty name="bg" property="r" />,<jsp:getProperty name="bg" property="g" />,<jsp:getProperty name="bg" property="b" />)">

<a href="/BGBean/?red=255&green=0&blue=0">Set bg color Red</a><br/>
<a href="/BGBean/?red=0&green=255&blue=0">Set bg color Green</a><br/>
<a href="/BGBean/?red=0&green=0&blue=255">Set bg color Blue</a><br/>
<a href="/BGBean/BG2">Next</a>
</body>
</html>