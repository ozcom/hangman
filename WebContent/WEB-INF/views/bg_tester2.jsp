<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="bg" class="se.ozcom.models.BG_Bean" scope="application "></jsp:useBean> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Page 2</title>
<link rel="stylesheet" href="./css/Name.css" />
</head>
<body style="background: rgb(<jsp:getProperty name="bg" property="r" />,<jsp:getProperty name="bg" property="g" />,<jsp:getProperty name="bg" property="b" />)">
<a href="/BGBean">Back</a>
</body>
</html>